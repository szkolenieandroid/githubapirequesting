package com.androidbootcamp.multipleactivities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidbootcamp.multipleactivities.model.GitHubUser;
import com.squareup.picasso.Picasso;

import java.util.List;

import twitter4j.Status;

/**
 * Created by dmitry on 06/11/14.
 */
public class TwittAdapter extends BaseAdapter {

    private final Context context;
    private final LayoutInflater inflater;

    public void setStatuses(List<Status> statuses) {
        this.statuses = statuses;
    }

    List<Status> statuses;

    public TwittAdapter(Context context, List<Status> statuses) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.statuses = statuses;
    }

    @Override
    public int getCount() {
        if(statuses == null) {
            return 0;
        }
        return statuses.size();
    }

    @Override
    public Object getItem(int position) {
        return statuses.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.github_uset_item, parent, false);
        }
        ImageView avatarImageView = (ImageView) convertView.findViewById(R.id.avatar_image);
        TextView name = (TextView) convertView.findViewById(R.id.user_name);
        Status status = statuses.get(position);

        name.setText(status.getText());
        Picasso.with(context)
                .load(status.getUser().getProfileImageURL())
                .into(avatarImageView);
        return convertView;
    }

}
