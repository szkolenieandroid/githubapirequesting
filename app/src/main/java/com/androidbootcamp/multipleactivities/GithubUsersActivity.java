package com.androidbootcamp.multipleactivities;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.androidbootcamp.multipleactivities.model.GitHubUser;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.List;

import twitter4j.Status;
import twitter4j.TwitterException;


public class GithubUsersActivity extends Activity {

    private TwittAdapter twittAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        initListView();
        requestApi();
    }

    private void initListView() {
        ListView listView = (ListView) findViewById(R.id.usersListView);
        twittAdapter = new TwittAdapter(this, null);
        listView.setAdapter(twittAdapter);
    }


    private void requestApi() {
        ApiRequestAsyncTask asyncTask = new ApiRequestAsyncTask();
        asyncTask.execute();

    }

    public void showTwitts(View view) {
        EditText tagText = (EditText) findViewById(R.id.tag_text);
        ApiRequestAsyncTask asyncTask = new ApiRequestAsyncTask();
        asyncTask.tag = tagText.getText().toString();
        asyncTask.execute();
    }

    class ApiRequestAsyncTask extends AsyncTask<Void, Void, Void> {

        List<twitter4j.Status> statuses;
        String tag = "@warsjawa";
        @Override
        protected Void doInBackground(Void... params) {
            TwitterConnector twitterConnector = new TwitterConnector();
            try {
                statuses = twitterConnector.getTwitterStatusList(tag);
            } catch (TwitterException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            presentTwitts(statuses);
        }
    }

    private void presentTwitts(List<Status> statuses) {
        twittAdapter.setStatuses(statuses);
        twittAdapter.notifyDataSetChanged();
    }

}
