package com.androidbootcamp.multipleactivities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidbootcamp.multipleactivities.model.GitHubUser;
import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.util.List;

import twitter4j.Status;

/**
 * Created by dmitry on 06/11/14.
 */
public class GithubUserAdapter extends BaseAdapter {

    List<GitHubUser> gitHubUsers;
    private final Context context;
    private final LayoutInflater inflater;

    public void setStatuses(List<Status> statuses) {
        this.statuses = statuses;
    }

    List<Status> statuses;

    public GithubUserAdapter(Context context, List<GitHubUser> gitHubUsers) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.gitHubUsers = gitHubUsers;
    }

    public void setGitHubUsers(List<GitHubUser> gitHubUsers) {
        this.gitHubUsers = gitHubUsers;
    }

//    @Override
//    public int getCount() {
//        if(gitHubUsers == null) {
//            return 0;
//        }
//        return gitHubUsers.size();
//    }

    @Override
    public int getCount() {
        if(statuses == null) {
            return 0;
        }
        return statuses.size();
    }

    @Override
    public Object getItem(int position) {
        return statuses.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.github_uset_item, parent, false);
        }
        ImageView avatarImageView = (ImageView) convertView.findViewById(R.id.avatar_image);
        TextView name = (TextView) convertView.findViewById(R.id.user_name);
//        GitHubUser gitHubUser = gitHubUsers.get(position);
        Status status = statuses.get(position);

        name.setText(status.getText());
        Picasso.with(context)
                .load(status.getUser().getProfileImageURL())
                .into(avatarImageView);
        return convertView;
    }

}
