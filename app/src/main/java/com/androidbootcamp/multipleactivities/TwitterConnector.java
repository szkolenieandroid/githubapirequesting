package com.androidbootcamp.multipleactivities;

import java.util.List;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

/**
 * Created by dmitry on 09/11/14.
 */
public class TwitterConnector {

    private final String LOGGED_USER_TOKEN = "631325406-qtYfFm2hFebeQlRxIjUY8lK9uBAOzgQu1cloziUH";
    private final String LOGGED_USER_SECRET = "Z7Ii7khJt3kzZLdscDqfsLb6jOWw0qwFljlAa5I";
    private final String TWITTER_CONSUMER_KEY = "OiwBeroMQvvOn9CzsR1ChQ";
    private final String TWITTER_CONSUMER_SECRET = "oaJ8GGAJXYcBsxcbClO2OGKLhHxCjkKncPSeOskE";


    private Twitter createTwitterRequester() {

        AccessToken accessToken = new AccessToken(LOGGED_USER_TOKEN, LOGGED_USER_SECRET);
        Twitter twitter = new TwitterFactory().getInstance();
        twitter.setOAuthConsumer(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET);
        twitter.setOAuthAccessToken(accessToken);

        return twitter;
    }


    public List<Status> getTwitterStatusList(String searchHashTag) throws TwitterException {

        Query query = new Query(searchHashTag);
        QueryResult twitResult = requestTwittersWithQuery(query);
        return twitResult.getTweets();

    }

    private QueryResult requestTwittersWithQuery(Query query) throws TwitterException {
        Twitter twitter = createTwitterRequester();
        QueryResult result = twitter.search(query);
        return result;

    }
}
